import json
import numpy as np
from PIL import Image

normal_size = 64
oversize_size = 128
compressed_row_count = 16
normal_row_count = 21
column_count = 13
animations = [
    # Cast
    [0, 1, 2, 3, 4, 5, 6],
    [7, 8, 9, 10, 11, 12, 13],
    [14, 15, 16, 17, 18, 19, 20],
    # Thrust
    [21, 22, 23, 24, 25, 26, 27, 28],
    [29, 30, 31, 32, 33, 34, 35, 36],
    [37, 38, 39, 40, 41, 42, 43, 44],
    # Walk
    [45, 46, 47, 48, 49, 50, 51, 52, 53],
    [54, 55, 56, 57, 58, 59, 60, 61, 62],
    [63, 64, 65, 66, 67, 68, 69, 70, 71],
    # Slash
    [72, 73, 74, 75, 76, 77],
    [78, 79, 80, 81, 82, 83],
    [84, 85, 86, 87, 88, 89],
    # Shoot
    [90, 91, 92, 93, 94, 95, 96, 97, 98, 99, 100, 101, 102],
    [103, 104, 105, 106, 107, 108, 109, 110, 111, 112, 113, 114, 115],
    [116, 117, 118, 119, 120, 121, 122, 123, 124, 125, 126, 127, 128],
    # Hurt
    [129, 130, 131, 132, 133, 134]
]

def uncompress(image, spritesheet):
    result = np.zeros(((compressed_row_count * oversize_size), (column_count * oversize_size)), dtype=image.dtype)
    for i, row in enumerate(animations):
        for j, sprite_index in enumerate(row):
            sprite = spritesheet["sprites"][sprite_index]
            left, top, width, height = sprite["box"]
            dx, dy = sprite["offset"]
            x = j * oversize_size + dx
            y = i * oversize_size + dy
            result[y:y+height,x:x+width] = image[top:top+height,left:left+width]
    return result

def generate_east_sprites(image):
    result = np.zeros(((normal_row_count * oversize_size), (column_count * oversize_size)), dtype=image.dtype)
    for i in range(normal_row_count):
        if i % 4 == 3:
            i_compressed = i // 4 * 3 + 1
            for j in range(column_count):
                result[i*oversize_size:(i+1)*oversize_size,j*oversize_size:(j+1)*oversize_size] = \
                    np.fliplr(image[i_compressed*oversize_size:(i_compressed+1)*oversize_size,j*oversize_size:(j+1)*oversize_size])
        else:
            i_compressed = i // 4 * 3 + (i % 4)
            result[i*oversize_size:(i+1)*oversize_size] = image[i_compressed*oversize_size:(i_compressed+1)*oversize_size]
    return result

def undersize(image):
    result = np.zeros(((normal_row_count * normal_size), (column_count * normal_size)), dtype=image.dtype)
    offset = (oversize_size - normal_size) // 2
    for i in range(normal_row_count):
        for j in range(column_count):
            result[i*normal_size:(i+1)*normal_size,j*normal_size:(j+1)*normal_size] = \
                image[i*oversize_size+offset:i*oversize_size+offset+normal_size,j*oversize_size+offset:j*oversize_size+offset+normal_size]
    return result

def apply_palette(image, palette):
    return palette[0,image]

if __name__ == '__main__':
    f = open('body_parts.json')
    body_parts = json.load(f)
    for body_part in body_parts:
        indices_path = body_part['indices']
        print(indices_path)
        indices = np.asarray(Image.open(indices_path))
        spritesheet_path = body_part['spritesheet']
        spritesheet = json.load(open(spritesheet_path))
        oversized_indices = uncompress(indices, spritesheet)
        oversized_full_indices = generate_east_sprites(oversized_indices)
        normal_indices = undersize(oversized_full_indices)
        for i, palette_path in enumerate(body_part['palettes']):
            palette = np.asarray(Image.open(palette_path))
            image = Image.fromarray(apply_palette(normal_indices, palette), mode='RGBA')
            image_path = 'normal/{}_{}.png'.format(indices_path[indices_path.find('/')+1:indices_path.rfind('.')], i)
            image.save(image_path, compress_level=9)
            oversized_image = Image.fromarray(apply_palette(oversized_full_indices, palette), mode='RGBA')
            oversized_image_path = 'oversized/{}_{}.png'.format(indices_path[indices_path.find('/')+1:indices_path.rfind('.')], i)
            oversized_image.save(oversized_image_path, compress_level=9)