# Structure

The `indices` folder contains grayscale images where the value of each pixel corresponds to the index of a color in a palette.

The `palettes` folder contains images with only one row of pixels.

The `images` folder contains the generated colored images.

`body_parts.json` matches the indices with the palettes. It is a list of objects with the following structure:
```json
{
    "indices": "<indices_image>",
    "palettes": ["<palette_image_1>", "...", "<palette_image_n>"]
}
```

`generate_colored_images.py` is a script that generates all the colored images described in `body_parts.json`.

# Generating the images

Make sure you have Python installed on your system, then run this command to install the dependencies:
```
pip install -r requirements.txt
```

Finally, you can generate the colored images:
```
python generate_colored_images.py
```